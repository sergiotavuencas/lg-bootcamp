#===============================================================================
#USAGE : ./restart.sh
#DESCRIPTION : Restart your own machine.
#TIPS: 
#===============================================================================
#
#!/bin/bash
#START
#echo "Do you want to reboot the computer?(y-yes)";
#read choice;
#
#if [[ "$choice" == "y" ]] || [[ "$choice" == "Y"  ]]
#	then
#		echo "\nThe computer will reboot in";
#
#		sleep 1;
#		echo "5";
#
#		sleep 1;
#		echo "4";
#
#		sleep 1;
#		echo "3";
#
#		sleep 1;
#		echo "2";
#
#		sleep 1;
#		echo "1";
#		
#		sleep 30;
#		echo "\nRebooting...\n";
#		reboot;
#	else
#		echo "\nA very sad face\n";
#fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
