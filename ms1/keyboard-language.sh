#===============================================================================
#USAGE : ./keyboard-language.sh language
#DESCRIPTION : Pass as an argument the target language.
#PARAMS: Language has to be the international language code like "us" or "sp"
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
#echo "Choose one of the two languages (us - us english | br - brazilian portuguese):";
#read language;
#
#if [[ "$language" == "us" ]]
#	then
#		echo "\nLayout changed to the US version\n";
#		setxkbmap -layout us;
#
#elif [[ "$language" == "br" ]]
#	then
#		echo "\nLayout changed to the BR version\n";
#		setxkbmap -layout br;
#
#else
#	echo "Ok";
#
#fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
