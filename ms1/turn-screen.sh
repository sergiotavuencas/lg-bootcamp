#===============================================================================
#USAGE : ./turnscreen.sh orientation
#DESCRIPTION : Pass as an argument de degrees of the rotation of the screen.
#PARAMS: orientation can be right, left, inverted
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
#echo "Choose the side wich you want to turn the screen(left | right | normal):";
#read choice;
#
#case "$choice" in
#	"left") echo "\nTurning the screen left...\n";
#		xrandr -o left;;
#
#	"right") echo "\nTurning the screen right...\n";
#		xrandr -o right;;
#		
#	"normal") echo "\nYou're boring...\n";
#		xrandr -o normal;;
#
#	*) echo "\nThis choice doesn't exist...\n";
#esac
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
