#===============================================================================
#USAGE : ./ssh-scp.sh hostname ipaddress password
#DESCRIPTION : Establishes ssh connection between two computer and monitor the packages. 
#PARAMS: Pass as arguments the hostname, ipaddress and password.
#TIPS: Make it simple, no need to overcomplicate it.
#===============================================================================
#
#!/bin/bash
#START
#echo "HOSTNAME: $1";
#echo "IPADDRESS: $2";
#echo "PASSWORD: $3";
#
#ssh $1@$2 << 'ENDSSH'
# ping -w 15 10.0.2.15 &	
# tcpdump -G 10 -W 1 -w ssh-ping-packages.pcap;
#ENDSSH
#
#user=($(whoami))
#ip=($(hostname -I))
# 
#sshpass -p $3 scp -r $1@$2:/home/$1/ssh-ping-packages.pcap $user@$ip:/home/$user;
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
