#===============================================================================
#USAGE : ./curl.sh
#DESCRIPTION : A menu to choose between making a http request or download a file using curl. Also to check if domain is up
#PARAMS: No params 
#TIPS: Instead of params, read user input
#===============================================================================
#
#!/bin/bash
#START
#echo "OPTION: $1"
#echo "DOMAIN: $2"
#
#if [ $1 -eq 1 ] || [ $1 -eq 2 ]
# then
# user=($(whoami))
# 
# if curl --head --silent --fail -X GET $2 > /dev/null
#  then
#  echo "The domains is Up!"
#  
#  if [ $1 -eq 1 ]
#   then
#   curl -i -H "Accept: application/json" -H "Content-type: application/json" $2 -o /home/$user/get-json-response.json
#   echo
#   echo "JSON file saved to /home/$user"
#  
#  else
#    (cd /home/$user && curl $2 -L -o file.zip); 
#    (cd /home/$user && unzip /home/$user/file.zip -d /home/$user/file);
#    echo;
#    echo "The .zip file was saved in /home/$user";
#    echo "The unzipped was saved in /home/$user/file";
#  fi
#  
# else
#  echo "The domain is down!"
#  exit
# fi
# 
#else
# echo "Wrong option!"
# exit
#fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
