#===============================================================================
#USAGE : ./shutdown.sh
#DESCRIPTION : Power off your machine using an script.
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
#echo "Do you really want to shutdown?(y-yes | n-no)";
#read op;
#case $op in
#	y) echo "\nshutting down...\n";
#		sleep 5;; shutdown -h now;;
#	n) echo "\nshutdown cancelled\n";;
#	*) echo "\ninvalid"\n;
#esac and END.
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
